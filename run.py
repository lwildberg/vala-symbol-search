#!/usr/bin/env python3

from pathlib import Path
import os
from sys import argv
from tempfile import mkdtemp
from shutil import rmtree


gitlab_modules = open("gitlab-modules.txt", "r").read().split("\n")
processed_dir = argv[1]

for m in gitlab_modules:
    module = m.split("\t")
    name = module[0]
    url = module[1]
    options = module[2]
    branch = module[3]

    clone_path = mkdtemp()

    os.environ["VALA_SYMBOLSEARCH_DIR"] = name
    os.environ["VALA_SYMBOLSEARCH_URL"] = url + "/-/blob/" + branch + "/"

    # create dir for valasymbols
    valasymbols_dir = Path.home() / ".valasymbols"
    Path.mkdir(valasymbols_dir / name, parents=True)
    # clone repo
    print("git clone --branch {0} {1} {2}".format(branch, url, clone_path))
    os.system("git clone --branch {0} {1} {2}".format(branch, url, clone_path))
    dir = os.getcwd()
    os.chdir(clone_path)
    # build module
    os.system("mkdir build")
    os.system("meson setup {0} build".format(options))
    os.system("meson compile -C build")

    os.chdir(dir)

    rmtree(clone_path)

# process symbols
os.system("symbol-search-process {0} {1}".format(os.fspath(valasymbols_dir), processed_dir))

