[Compact (opaque=true)]
class Symbol {
    public Symbol (string symbol_type, string symbol_name) {
        this.locations = new Location[] {};
        this.symbol_name = symbol_name;
        switch (symbol_type) {
            case "ValaMethod":
                this.symbol_type = "Method";
                break;
            case "ValaProperty":
                this.symbol_type = "Property";
                break;
            case "ValaField":
                this.symbol_type = "Field";
                break;
            case "ValaClass":
                this.symbol_type = "Constructor";
                break;
            case "ValaEnumValue":
                this.symbol_type = "Enum Value";
                break;
            case "ValaSignal":
                this.symbol_type = "Signal";
                break;
            default:
                this.symbol_type = symbol_type;
                break;
        }
    }

    public void add_location (Location l) {
        this.locations += l;
    }

    public string get_all_locations () {
        string result =
"""---
layout: page
title: %s
---

**Type:** %s

Module | Source File | Start Line | Start Column | End Line | End Column
-------|-------------|------------|--------------|----------|-----------
""".printf (this.symbol_name, this.symbol_type);

        foreach (Location loc in this.locations) {
            result += "%s|[%s](%s)|%s|%s|%s|%s\n".printf(
                loc.module,
                loc.path.replace ("../", ""),
                loc.url+loc.path.replace ("../", "")+"#L"+loc.line_begin.to_string (),
                loc.line_begin.to_string (),
                loc.column_begin.to_string (),
                loc.line_end.to_string (),
                loc.column_end.to_string ()
            );
        }

        return result;
    }

    public string symbol_type { get; set; }
    public string symbol_name { get; set; }

    private Location[] locations;
}

struct Location {
    string module;
    string path;
    int line_begin;
    int column_begin;
    int line_end;
    int column_end;
    string url;
}

void main (string[] args) {
    HashTable<string, Symbol?> symbols = new HashTable<string, Symbol?> (str_hash, str_equal);

    File search_dir = File.new_for_commandline_arg (args[1]);
    FileEnumerator enumerator = search_dir.enumerate_children ("standard::*", NONE);
    FileInfo? info = null;
    while ((info = enumerator.next_file (null)) != null) {
		File subdir = search_dir.resolve_relative_path (info.get_name ());
		string module_name = subdir.get_basename ();
		FileEnumerator subdir_enumerator = subdir.enumerate_children ("standard::*", NONE);
		FileInfo? subdir_info = null;
		while ((subdir_info = subdir_enumerator.next_file (null)) != null) {
		    File data = subdir.resolve_relative_path (subdir_info.get_name ());
		    foreach (string line in ((string) Bytes.unref_to_data (data.load_bytes ())).split ("\n")) {
		        string[] parts = line.split (",");
		        if (parts.length != 8) {
		            continue;
		        }

		        Location location = {
		            module_name,
                    parts[2],
                    int.parse (parts[3]),
                    int.parse (parts[4]),
                    int.parse (parts[5]),
                    int.parse (parts[6]),
                    parts[7]
		        };

		        if (!(parts[1] in symbols)) {
		            symbols[parts[1]] = new Symbol (parts[0], parts[1]);
		        }
		        symbols[parts[1]].add_location (location);
		    }
		}
    }

    File dest_dir = File.new_for_commandline_arg (args[2]);
    File search_file = dest_dir.resolve_relative_path ("search.json");
    string search_content = "---\n---\n[";

    uint n_symbols = symbols.length;
    symbols.foreach ((key, val) => {
        File symfile = dest_dir.resolve_relative_path (key+".md");
        symfile.replace_contents (val.get_all_locations ().data, null, false, FileCreateFlags.NONE, null);
        search_content += "\n  {\"title\":\"%s\",\"url\":\"{{ site.baseurl }}%s\"}".printf (key, key+".html");
        if (n_symbols != 1) {
            search_content += ",";
        }
        n_symbols--;
    });

    search_content += "\n]";

    search_file.replace_contents (search_content.data, null, false, FileCreateFlags.NONE, null);
}
